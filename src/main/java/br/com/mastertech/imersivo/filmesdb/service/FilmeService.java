package br.com.mastertech.imersivo.filmesdb.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.mastertech.imersivo.filmesdb.model.Filme;
import br.com.mastertech.imersivo.filmesdb.model.FilmeDTO;
import br.com.mastertech.imersivo.filmesdb.repository.FilmeRepository;

@Service
public class FilmeService {
	
	@Autowired
	private FilmeRepository filmeRepository;
	
	public Iterable<Filme> obterFilmes() {
		System.out.println("Chamaram o listar filmes");

		return filmeRepository.findAll();
	}
	
	public Iterable<FilmeDTO> obterFilmes(Long diretorId) {
		return filmeRepository.findAllByDiretor_Id(diretorId);
	}
	
	public void criarFilme(Filme filme) {
		filmeRepository.save(filme);
		System.out.println("Chamaram o criar do " + filme.getTitulo());
	}

	public void apagarFilme(Long id) {
		Filme filme = encontraOuDaErro(id);
		filmeRepository.delete(filme);
	}

	public Filme editarFilme(Long id, Filme filmeAtualizado) {
		Filme filme = encontraOuDaErro(id);
		
		filme.setGenero(filmeAtualizado.getGenero());
		
		return filmeRepository.save(filme);
	}
	
	public Filme encontraOuDaErro(Long id) {
		Optional<Filme> optional = filmeRepository.findById(id);
		
		if(!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Filme não encontrado");
		}
		
		return optional.get();
	}

}
