package br.com.mastertech.imersivo.filmesdb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mastertech.imersivo.filmesdb.model.Diretor;
import br.com.mastertech.imersivo.filmesdb.model.DiretorDTO;
import br.com.mastertech.imersivo.filmesdb.model.FilmeDTO;
import br.com.mastertech.imersivo.filmesdb.service.DiretorService;

@RestController
public class DiretorController {
	
	@Autowired
	private DiretorService diretorService;
	
	@GetMapping("/diretor")
	public Iterable<Diretor> listarDiretores() {
		return diretorService.obterDiretores();
	}
	
	@GetMapping("/diretor/{id}/filmes")
	public DiretorDTO listarFilmes(@PathVariable Long id) {
		return diretorService.obterDiretor(id);
	}
	
	@PostMapping("/diretor")
	@ResponseStatus(code = HttpStatus.CREATED)
	public void criarDiretor(@RequestBody Diretor diretor) {
		diretorService.criarDiretor(diretor);
	}
	
	@DeleteMapping("/diretor/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void apagarDiretor(@PathVariable Long id) {
		diretorService.apagarDiretor(id);
	}
	
}