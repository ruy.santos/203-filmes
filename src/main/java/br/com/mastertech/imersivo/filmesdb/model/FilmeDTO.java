package br.com.mastertech.imersivo.filmesdb.model;

import java.util.Date;

public interface FilmeDTO {

	Long getId();
	
	String getTitulo();
	
	String getGenero();
	
	Date getData();

}
